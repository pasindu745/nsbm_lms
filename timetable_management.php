<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Time Table Managemtnet</title>
    <?php include 'includes/head.php'; ?><!--css files-->
</head>
<body>
    <?php include 'includes/navbar.php';?><!--navbarphp-->
    <br><br><br><br>

    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <?php include 'includes/panel.php';?><!-- pills-->
            </div>


            <div class="col-md-9">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#add" data-toggle="tab">Add time table</a> </li>
                    <li><a href="#view" data-toggle="tab">View uploaded</a> </li>
                </ul>

               <div class="tab-content">
                   <div class="tab-pane active" id="add"><br>
                       <div class="panel panel-info">
                           <div class="panel-heading">Add Timtable</div>
                           <div class="panel-body">
                               <form class="form form-horizontal" method="post" role="form" action="">
                                   <label>Topic:</label>
                                   <input type="text" class="form-control" name="topic">

                                   <label>Batch:</label>
                                   <select class="form-control" name="batch">
                                       <option disabled selected>Select the batch</option>
                                       <option>15.1</option>
                                   </select>

                                   <label>Course:</label>
                                   <select class="form-control" name="course">
                                       <option disabled selected>Select the Course</option>
                                   </select>

                                   <label>Upload File:</label>
                                   <input type="file" class="form-control" name="file">
                                   <br>

                                   <button type="submit" class="btn btn-default">Submit</button>
                               </form>
                           </div>
                       </div>
                   </div>

                   <div class="tab-pane" id="view">
                       <table class="table table-bordered table-responsive">
                           <thead>
                           <tr>
                               <th>Id</th>
                               <th>Topic</th>
                               <th>File</th>
                               <th>Batch</th>
                               <th>Course</th>
                               <th>Uploaded By</th>
                               <th>Actions</th>
                           </tr>
                           </thead>

                           <tbody>
                           <tr>
                               <td></td><!-- there must be two buttons and another form for edit timetable-->
                           </tr>
                           </tbody>
                       </table>
                   </div>
               </div>
            </div>
        </div>
    </div>




<script src="js/jquery-1.11.3.min.js"></script><!--Javascript file-->
<script src="js/bootstrap.min.js"></script>
</body>
</html>