<?php
error_reporting(E_ALL ^ E_NOTICE);
session_start();
include 'functions/connection.php';
require 'functions/functions.php' ;

?>

<html>
<head>
    <title>Lecturer Login</title>
    <?php include 'includes/head.php'; ?><!--css files-->
</head>
<body>

    <?php include 'includes/navbar_lec.php'; ?><!-- nav bar for lecturers login and sign up pages-->
    <br><br><br><br>

    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-info">
                    <div class="panel-heading"><strong>Lecturer Login</strong></div>
                    <div class="panel-body">
                        <?php
                        $submit=$_POST['submit'];
                        $email=$_POST['email'];
                        $pass=$_POST['pass'];

                        if($submit)
                        {
                            if($email && $pass)
                            {
                                $email=mysql_real_escape_string($email);

                               if(lec_exsist($email)==true)
                               {
                                    echo "User does not exsist";
                               }
                                else
                                {
                                    $email=mysql_real_escape_string($email);
                                    $pass=md5($pass);
                                    $login=lec_login($email , $pass);

                                    if($login==false)
                                    {
                                        echo "Incorrect Email or Incorrect Password";
                                    }
                                    else
                                    {
                                        header('location:lec_profile.php');
                                    }
                                }
                            }
                            else
                            {
                                echo "Please enter Email and Password";
                            }
                        }

                        ?>

                        <form class="form-horizontal" method="post" role="form" action="lec_login.php">
                            <label>Email:</label>
                            <input type="email" class="form-control" name="email">

                            <label>Password:</label>
                            <input type="password" class="form-control" name="pass">

                            <br>
                            <input type="submit" name="submit" value="Login" class="btn btn-info">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

<br><br><br><br><br><br>


<?php include 'includes/footer.php';?>

<script src="js/jquery-1.11.3.min.js"></script><!--Javascript file-->
<script src="js/bootstrap.min.js"></script>
</body>
</html>