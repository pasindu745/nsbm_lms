<html>
<head>
	<title>Contact us</title>
	<?php include 'includes/head.php'; ?><!--css files-->
</head>

<body>
	<?php include 'includes/navbar.php';?><!--navbarphp-->
	<br><br><br><br>


    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-info">
                    <div class="panel-heading"><strong>Contact Us</strong></div>
                    <div class="panel-body">
                        <form class="form-horizontal" method="post" role="form" action="">
                            <label>Name:</label>
                            <input type="text" class="form-control" name="name">

                            <label>Email:</label>
                            <input type="email" class="form-control" name="email">

                            <label>Message:</label>
                            <textarea class="form-control" name="message" rows="12"></textarea><br>

                            <button type="submit" class="btn btn-info">Send</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>




<br>
	<?php include 'includes/footer.php';?>

<script src="js/jquery-1.11.3.min.js"></script><!--Javascript file-->
<script src="js/bootstrap.min.js"></script>	
</body>
</html>