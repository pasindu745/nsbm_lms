<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Manage lecturers</title>
    <?php include 'includes/head.php'; ?><!--css files-->
</head>

<body>
    <?php include 'includes/navbar.php';?><!--navbarphp-->
    <br><br><br><br>

    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <?php include 'includes/panel.php';?>
            </div>

             <div class="col-md-9">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#lecturers" data-toggle="tab">Lecturers </a></li>
                </ul>

                 <div class="tab-content">
                     <div class="tab-pane active" id="lecturers"><br>
                         <table class="table table-bordered table-responsive">
                             <thead>
                             <tr>
                                 <th>Lecturer Id</th>
                                 <th>Lecturer Name</th>
                                 <th>Leturer Email</th>
                                 <th>Remove</th>
                             </tr>
                             </thead>
                             <tbody>
                             <tr>
                                 <td></td>
                             </tr>
                             </tbody>
                         </table>
                     </div>
                 </div>
            </div>
        </div>
    </div>





<script src="js/jquery-1.11.3.min.js"></script><!--Javascript file-->
<script src="js/bootstrap.min.js"></script>
</body>
</html>