<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Slides Managment</title>
    <?php include 'includes/head.php'; ?><!--css files-->
</head>
<body>
    <?php include 'includes/navbar.php';?><!--navbarphp-->
    <br><br><br><br>

    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <?php include 'includes/panel.php';?><!-- pills-->
            </div>

            <div class="col-md-9">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#slides" data-toggle="tab">Slides</a> </li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane active" id="slides"><br>
                        <table class="table table-bordered table-responsive">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Lecturer</th>
                                <th>Date</th>
                                <th>Course</th>
                                <th>Faculty</th>
                                <th>Year</th>
                                <th>File</th>
                                <th>Delete</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <script src="js/jquery-1.11.3.min.js"></script><!--Javascript file-->
    <script src="js/bootstrap.min.js"></script></body>
</html>