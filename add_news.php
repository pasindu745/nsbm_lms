<html>
<head>
	<title>add news</title>
	<?php include 'includes/head.php'; ?><!--css files-->
</head>

<body>
	<?php include 'includes/navbar.php';?><!--navbarphp-->

	<br><br><br>
    <h1 align="center">--Admin Panel--</h1><br><br>

  	<div class="container">
  		<div class="row">
  			<div class="col-md-2"><!-- for buttons-->
  				<?php include 'includes/panels.php'; ?>
  			</div>


  			<div class="col-md-10">
  				<form action="" class="form-horizontal" method="post" role="form">
  					<div class="form-group">
  						<label for="heading" class="col-sm-3 control-label">Heading:</label>
  						<div class="col-md-6">
  							<input type="text" class="form-control" id="heading" placeholder="Heading" name="heading">
  						</div>
					</div>


  					<div class="form-group">
  						<label for="content" class="col-sm-3 control-label">Content:</label>
  						<div class="col-md-8">
  							<textarea class="form-control" rows="10"  placeholder="content" name="content"></textarea>
  						</div>
  					</div>

            <div class="form-group">
              <label for="image" class="col-sm-3 control-label">Image:</label>
              <div class="col-md-8">
                <input type="file" class="form-control" id="image"  name="image">
              </div>
            </div>
  					
            <div class="form-group">
                  <div class="col-sm-offset-3 col-sm-10">
                  <button type="submit" class="btn btn-default">Submit</button>
                 </div>
              </div>
  				</form>
  			</div>
  		</div>
  	</div>



<?php include 'includes/footer.php';?><!--this is footer-->


<script src="js/jquery-1.11.3.min.js"></script><!--Javascript file-->
<script src="js/bootstrap.min.js"></script>	
</body>
</html>