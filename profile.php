<html>
<head>
    <title>Profile</title>
    <?php include 'includes/head.php'; ?><!--css files-->
</head>

<body>

    <?php include 'includes/navbar_login.php'; ?>

    <br><br><br><br>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <!--Tab Navigation-->
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#profile" data-toggle="tab">Profile</a> </li>
                    <li><a href="#lectures" data-toggle="tab">Lectures</a> </li>
                    <li><a href="#assignments" data-toggle="tab">Assignments</a> </li>
                    <li><a href="#timetable" data-toggle="tab">Timetable</a> </li>
                    <li><a href="#edit" data-toggle="tab">Edit Profile</a> </li>
                </ul>

                <!--Tab Contents-->
                <div class="tab-content">
                    <div class="tab-pane active" id="profile"><br>
                        <div class="col-md-10">
                            <div class="panel panel-info">
                                <div class="panel-heading">User Information</div>
                                <div class="panel-body">
                                    <pre>
                                         <p>Name:</p>
                                         <p>Index Number:</p>
                                         <p>Username:</p>
                                         <p>Email:</p>
                                         <p>School:</p>
                                         <p>Course:</p>
                                        <p>Batch:</p>
                                    </pre>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id="lectures">
                        <div class="col-md-8">
                            <h3><a href="">Year 1</a> </h3>
                            <h3><a href="">Year 2</a> </h3>
                            <h3><a href="">Year 3</a> </h3>
                        </div>
                    </div>

                    <div class="tab-pane" id="assignments">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Date</th>
                                <th>Assignment</th>
                                <th>Module</th>
                                <th>Submission Date</th>
                                <th>Download</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th></th>
                                </tr>
                            </tbody>
                        </div>

                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Date</th>
                                <th>Assignment</th>
                                <th>Module</th>
                                <th>Upload</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="tab-pane" id="timetable">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Date</th>
                                <th>Time Table</th>
                                <th>Download</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="tab-pane" id="edit">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6"><br>
                                    <div class="panel panel-info">
                                        <div class="panel-heading">
                                            Edit Personal Info
                                        </div>
                                        <div class="panel-body">
                                            <form class="form-horizontal"  method="post" action="" role="form">
                                                <label>First Name:</label>
                                                <input type="text" class="form-control" name="fname">
                                                <label>Last Name:</label>
                                                <input type="text" class="form-control" name="lname">
                                                <label>E mail:</label>
                                                <input type="email" class="form-control" name="email">
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6"><br>
                                    <div class="panel panel-info">
                                        <div class="panel-heading">
                                            Edit Password
                                        </div>
                                        <div class="panel-body">
                                            <form class="form-horizontal" action="" method="post" role="form">
                                                <label>Current Password:</label>
                                                <input type="password" class="form-control" name="cpass">
                                                <label>New Password:</label>
                                                <input type="password" class="form-control" name="npass">
                                                <label>Re-Type New Password:</label>
                                                <input type="password" class="form-control" name="rnpass">
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include 'includes/footer.php';?>

    <script src="js/jquery-1.11.3.min.js"></script><!--Javascript file-->
    <script src="js/bootstrap.min.js"></script>
</body>
</html>