<html>
<head>
	<title>edit admin</title>
	<?php include 'includes/head.php'; ?><!--css files-->
</head>

<body>
	<?php include 'includes/navbar.php';?><!--navbarphp-->
	<br><br><br>
    <h1 align="center">--Edit Admin Profiles--</h1><br><br>

    <div class="container">
    	<div class="row">
    		<div class="col-md-2">
    			<?php include 'includes/panels.php'; ?>
    		</div>

    		<div class="col-md-10">
    			<form action="" class="form-horizontal" method="post" role="form">

    				<div class="form-group">
    					<label for="user" class="col-sm-3 control-label">User name:</label>
    					<div class="col-md-6">
    						<input type="text" class="form-control" id="user" placeholder="Username" name="username">
    					</div>
    				</div>

    				<div class="form-group">
    					<label for="name" class="col-sm-3 control-label">Name:</label>
    					<div class="col-md-6">
    						<input type="text" class="form-control" id="name" placeholder="Name" name="name">
    					</div>
    				</div>

    				<div class="form-group">
    					<label for="des" class="col-sm-3 control-label">Description:</label>
    					<div class="col-md-6">
    						<textarea class="form-control" rows="10"  placeholder="Description" name="description"></textarea>
    					</div>
    				</div>

    				<div class="form-group">
    					<label for="image" class="col-sm-3 control-label">Image:</label>
    					<div class="col-md-6">
    						<input type="file" class="form-control" id="image" name="image">
    					</div>
    				</div>

    				<div class="form-group">
    					<div class="col-sm-offset-3 col-sm-10">
                  			<button type="submit" class="btn btn-default">Submit</button>
                 		</div>
    				</div>
    			</form>
    		</div>
    	</div>
    </div>






<script src="js/jquery-1.11.3.min.js"></script><!--Javascript file-->
<script src="js/bootstrap.min.js"></script>	
</body>
</html>