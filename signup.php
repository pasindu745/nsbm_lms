<?php
error_reporting(E_ALL ^ E_NOTICE);
include 'functions/connection.php';
?>


<html>
<head>
	<title>Signup</title>
	<?php include 'includes/head.php'; ?><!--css files-->
</head>


<body>

	<?php include 'includes/navbar.php'; ?><!--navbarphp-->
	
	<br><br><br><br>
	
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
                <div class="panel panel-info">
                    <div class="panel-heading"><strong>Enter Your Details for Sign up</strong></div>
                    <div class="panel-body">

                        <?php
                        $submit=$_POST['submit'];
                        $fname=$_POST['fname'];
                        $lname=$_POST['lname'];
                        $index=$_POST['index'];
                        $email=$_POST['email'];
                        $remail=$_POST['remail'];
                        $school=$_POST['school'];
                        $course=$_POST['course'];
                        $batch=$_POST['batch'];
                        $pass=$_POST['pass'];
                        $repass=$_POST['repass'];
                        $date=date("y-m-d");
                        //generating random number for email activation
                        $emailcode=rand(23456789 , 98765432);


                        $check_email=mysql_query("SELECT email FROM student_signup WHERE email='$email'");
                        $count=mysql_num_rows($check_email);

                        if($submit)
                        {
                            if($fname && $lname && $index && $email && $remail && $school && $course && $batch && $pass && $repass)
                            {
                                if(strlen($fname)>20)
                                {
                                    echo "Max value for first name is 20 characters";
                                }

                                else
                                {
                                    if(strlen($lname)>30)
                                    {
                                        echo "Max value for last name is 30 characters";
                                    }
                                    else
                                    {
                                        if(strlen($index)>20)
                                        {
                                            echo "Max value for index number is 20 characters";
                                        }
                                        else
                                        {
                                            if(strlen($email)>50)
                                            {
                                                echo "Max value for Email is 50 characters";
                                            }
                                            else
                                            {
                                                if($email != $remail)
                                                {
                                                    echo "Emails do not match";
                                                }
                                                else
                                                {
                                                    if($count != 0)
                                                    {
                                                        echo "Email has already taken";
                                                    }
                                                    else
                                                    {
                                                        if(strlen($pass)>20 || strlen($pass)<6)
                                                        {
                                                            echo "Password must between 6-20 characters";
                                                        }
                                                        else
                                                        {
                                                            if($pass != $repass)
                                                            {
                                                                echo "Passwords do not match";
                                                            }
                                                            else
                                                            {
                                                                $pass=md5($pass);

                                                                //query

                                                                $insert=mysql_query("INSERT INTO student_signup VALUES ('' , '$fname' , '$lname' , '$index' , '$email' , '$school' , '$course' , '$batch' , '$pass' , '$emailcode' , '' , '$date')");
                                                                echo "Success. Check your email to activate your account!";

                                                                //this gives us last id we entered to the database
                                                                $last_id=mysql_insert_id();

                                                                //sending email for the activation code

                                                                $to=$email;
                                                                $subject = "Email Activation";
                                                                $header="FROM: LMS.COM";
                                                                $body="Hello $fname \n\n You need to activate your account with the link below:\n\n http://localhost/nsbm_lms_assignment/activate.php?id=$last_id&mailcode=$emailcode";

                                                                //function to send the email
                                                                mail($to, $subject, $body, $header);

                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                echo "please fill all fields";
                            }

                        }
                        ?>

                        <form class="form-horizontal" method="post" role="form" action="signup.php">

                            <label>First Name:</label>
                            <input type="text" class="form-control" name="fname">

                            <label>Last Name:</label>
                            <input type="text" class="form-control" name="lname">

                            <label>Index Number:</label>
                            <input type="text" class="form-control" name="index">

                            <label>E Mail:</label>
                            <input type="email" class="form-control" name="email">

                            <label>Re Enter E Mail:</label>
                            <input type="email" class="form-control" name="remail">

                            <label>School:</label>
                            <select class="form-control"  name="school">
                                <option disabled selected> Choose the School </option>
                                <option>Computing</option>
                                <option>Management</option>
                                <option>Engineering</option>
                            </select>

                            <label>Course:</label>
                            <select class="form-control" required name="course">
                                <option disabled selected>Choose your Course</option>
                                <option>BSc in Business Management(Human Resource Management)(Special)</option>
                                <option>BSc in Business Management(Logistics Management)(Special)</option>
                                <option>BSc in Business Management(Project Management)(Special)</option>
                                <option>BSc in Business Management(Industrial Management)(Special)</option>
                                <option>BSc(Hons) Marketing Management</option>
                                <option>BSc Management Information Systems</option>
                                <option>BSc(Hons)Computing</option>
                                <option>BSc(Hons)Software Engineering</option>
                                <option>BSc(Hons)Computer Networks</option>
                                <option>BSc(Hons)Computer Security</option>
                            </select>

                            <label>Batch:</label>
                            <select class="form-control" name="batch">
                                <option disabled selected>Choose your batch</option>
                                <option>15.1</option>
                                <option>15.2</option>
                            </select>

                            <label>Password:</label>
                            <input type="password" class="form-control" name="pass">

                            <label>Re enter Password:</label>
                            <input type="password" class="form-control" name="repass">

                            <br>

                           <input type="submit" name="submit" value="Sign Up" class="btn btn-info">
                        </form>
                    </div>
                </div>
			</div>
		</div>


	</div>



<?php include 'includes/footer.php';?>

<script src="js/jquery-1.11.3.min.js"></script><!--Javascript file-->
<script src="js/bootstrap.min.js"></script>
</body>
</html>