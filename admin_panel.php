<?php
session_start();
?>

<html>
<head>
    <title>Admin Panel</title>
    <?php include 'includes/head.php'; ?><!--css files-->
</head>

<body>
    <?php include 'includes/navbar.php';?><!--navbarphp-->
    <br><br><br><br>


    <div class="container">
        <div class="row">
            <div class="col-md-3 col-md-offset-9">
                <?php
                echo "You are logged in as : \n". $_SESSION['name'];
                ?>
                <br>
                <a href="log_out.php">logout</a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3"><!---pills-->
                <?php include 'includes/panel.php';?>
            </div>

            <div class="col-md-9">
                <h3>Total Number of Admins:<span class="badge">0</span></h3>

                <h3>Total Number of Students:<span class="badge">0</span> </h3>

                <h3>Total Number of Lecturers:<span class="badge">0</span></h3>

                <h3>Total Number of Societies:<span class="badge">0</span></h3>
            </div>
        </div>
    </div>




    <script src="js/jquery-1.11.3.min.js"></script><!--Javascript file-->
    <script src="js/bootstrap.min.js"></script>
</body>
</html>