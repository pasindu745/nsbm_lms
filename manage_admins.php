<html>
<head>
    <title>Manage Admins</title>
    <?php include 'includes/head.php'; ?><!--css files-->
</head>

<body>
<?php include 'includes/navbar.php';?><!--navbarphp-->
<br><br><br><br>

<div class="container">
    <div class="row">
        <div class="col-md-3"><!---pills-->
            <?php include 'includes/panel.php';?>
        </div>

        <div class="col-md-9">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#admins" data-toggle="tab">Admins</a></li>
                <li><a href="#add" data-toggle="tab">Add New</a> </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="admins"><!-- aadmin information-->
                    <div class="col-md-9"><br>
                        <table class="table table-bordered table-responsive">
                            <thead>
                            <tr>
                                <th>Admin Id</th>
                                <th>Admin Name</th>
                                <th>Username</th>
                                <th>Email</th>
                                <th>Profile Pic</th>
                                <th>Date</th>
                                <th>Remove</th>
                            </tr>
                            </thead>

                            <tbody>
                            <tr>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="tab-pane" id="add"><!-- Add new admins-->
                    <div class="col-md-8 col-md-offset-1"><br>
                        <div class="panel panel-info">
                            <div class="panel-heading">Add new Admin</div>
                            <div class="panel-body">
                                <form class="form-horizontal" method="post" role="form" action="">
                                    <label>First name:</label>
                                    <input type="text" class="form-control" name="fname">

                                    <label>Last Name:</label>
                                    <input type="text" class="form-control" name="lname">

                                    <label>E mail:</label>
                                    <input type="email" class="form-control" name="email">

                                    <label>Password:</label>
                                    <input type="password" class="form-control" name="pass">

                                    <label>Repeat Password:</label>
                                    <input type="password" class="form-control" name="repass">

                                    <label>Admin Type:</label>
                                    <select class="form-control" name="type">
                                        <option disabled selected>Select Admin Type</option>
                                        <option>Alpha</option>
                                        <option>Standard</option>
                                    </select>

                                    <label>Profile Picture:</label>
                                    <input type="file" class="form-control" name="pic">
        `                              <br>
                                    <button type="submit" class="btn btn-default">Submit</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




<script src="js/jquery-1.11.3.min.js"></script><!--Javascript file-->
<script src="js/bootstrap.min.js"></script>
</body>
</html>