<nav class="navbar navbar-inverse navbar-fixed-top" data-spy="affix" data-offset-top="50">
    <div class="container-fluid">
        <a href="index.php" class="navbar-brand"><b>LMS.com</b></a>

        <button class="navbar-toggle" data-toggle="collapse" data-target=".navHeaderCollapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button><!--Responsive navbar-->

        <div class="collapse navbar-collapse navHeaderCollapse">
            <ul class="nav navbar-nav "><!-- nav bar left items-->
                <li><a href="index.php"><b>Home</b></a></li>
                <li><a href="news.php"><b>News & Events</b></a></li>
                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><b>Faculty</b><b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="login.php">Faculty of Computing</a></li>
                        <li><a href="login.php">Faculty of Management</a></li>
                        <li><a href="login.php">Faculty of Engineering</a></li>
                    </ul>
                </li>
                <li><a href="contact_us.php"><b>ContactUs</b></a></li>
                <li><a href="about_us.php"><b>About Us</b></a></li>
            </ul><!-- nav bar left items-->

            <ul class="nav navbar-nav navbar-right"><!-- nav bar right items-->
                <li><a href="profile.php"><b>Profile</b> <span class="glyphicon glyphicon-user" aria-hidden="true"></span></a></li>
                <li><a href="#"><b>Log Out</b> <span class="glyphicon glyphicon-log-out"></span></a></li>
            </ul><!-- nav bar right items-->
        </div>
    </div>
</nav>