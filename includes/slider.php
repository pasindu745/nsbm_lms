<div id="slider" class="carousel slide" data-ride="carousel"><!--slider-->
  
  <ol class="carousel-indicators"><!-- Indicators -->
    <li data-target="#slider" data-slide-to="0" class="active"></li>
    <li data-target="#slider" data-slide-to="1"></li>
    <li data-target="#slider" data-slide-to="2"></li>
    <li data-target="#slider" data-slide-to="3"></li>
  </ol><!-- Indicators -->

  
  <div class="carousel-inner"><!--images-->
    <div class="item active">
      <img class="img-responsive" src="images/vission.jpg" alt="motivation">
    </div>

    <div class="item">
      <img class="img-responsive" src="images/vission2.jpg" alt="motivation">
    </div>

    <div class="item">
      <img class="img-responsive" src="images/vission3.jpg" alt="motivation">
    </div>

    <div class="item">
      <img class="img-responsive" src="images/vission4.jpg" alt="motivation ">
    </div>
  </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#slider" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
  </a>
  <a class="right carousel-control" href="#slider" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
  </a>
</div>