<?php
error_reporting(E_ALL ^ E_NOTICE);
session_start();
include 'functions/connection.php';
require 'functions/functions.php';
?>


<html>
<head>
	<title>log in</title>
	<?php include 'includes/head.php'; ?><!--css files-->
</head>

<body>
	<?php include 'includes/navbar.php';?><!--navbarphp-->
  
  <br><br><br><br>

	<div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-info">
                    <div class="panel-heading"><strong>Log In</strong></div>
                    <div class="panel-body">

                        <?php
                        $submit=$_POST['submit'];
                        $email=$_POST['email'];
                        $pass=$_POST['pass'];

                        if($submit)
                        {
                            if($email && $pass)
                            {
                                $email=mysql_real_escape_string($email);

                                if(student_exsist($email)==true)
                                {
                                    echo "User does not exsist";
                                }
                                else
                                {
                                    $email=mysql_real_escape_string($email);
                                    $pass=md5($pass);
                                    $login=student_login($email , $pass);

                                    if($login == false)
                                    {
                                        echo "Incorrect Username or Password";
                                    }
                                    else
                                    {
                                        $email=mysql_real_escape_string($email);

                                        if(student_active($email)== true)
                                        {
                                            echo "Please active your account by confirming your Email";
                                        }
                                        else
                                        {
                                            header('location:profile.php');
                                        }
                                    }
                                }
                            }
                            else
                            {
                                echo "Please fill all fields";
                            }
                        }

                        ?>

                        <form class="form-horizontal" method="post" role="form" action="login.php">
                            <label>Email:</label>
                            <input type="email" class="form-control" name="email">

                            <label>Password:</label>
                            <input type="password" class="form-control" name="pass">

                            <label>
                            <input type="checkbox">Remember Me
                            </label><br><br>

                            <input type="submit" name="submit" class="btn btn-info" value="LogIn"><br><br>

                            <a href="">Forgot Password?</a><br><br>

                            <a href="signup.php" class="btn btn-info">Sign Up</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
	</div>
  </div>
  <br> <br> <br> <br> <br>
   
<?php include 'includes/footer.php';?><!--this is footer-->


 
   

	
<script src="js/jquery-1.11.3.min.js"></script><!--Javascript file-->
<script src="js/bootstrap.min.js"></script>
</body>
</html>