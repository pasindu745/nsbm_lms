<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>News Management</title>
    <?php include 'includes/head.php'; ?><!--css files-->
</head>
<body>

    <?php include 'includes/navbar.php';?><!--navbarphp-->
    <br><br><br><br>

    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <?php include 'includes/panel.php';?><!-- pills-->
            </div>

            <div class="col-md-9">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#add" data-toggle="tab">Add News</a> </li>
                    <li><a href="#view" data-toggle="tab">View</a> </li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane active" id="add"><br>
                        <div class="panel panel-info">
                            <div class="panel-heading">Add News</div>
                            <div class="panel-body">
                                <form class="form-horizontal" method="post" role="form" action="">
                                    <label>Heading:</label>
                                    <input type="text" class="form-control" name="heading">

                                    <label>Content:</label>
                                    <textarea class="form-control" rows="15" name="content"></textarea>

                                    <label>Upload picture:</label>
                                    <input type="file" class="form-control" name="pic"><br>

                                    <button type="submit" class="btn btn-default">Submit</button>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id="view"><br>
                        <table class="table table-bordered table-responsive">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Heading</th>
                                <th>Date</th>
                                <th>Uploaded by</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <script src="js/jquery-1.11.3.min.js"></script><!--Javascript file-->
    <script src="js/bootstrap.min.js"></script>
</body>
</html>