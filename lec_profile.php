<html>
<head>
    <title>Lecturer Profile</title>
    <?php include 'includes/head.php'; ?><!--css files-->
</head>

<body>
    <?php include 'includes/navbar_lec_login.php'; ?>
    <br><br><br><br>

    <div class="contianer">
        <div class="row">
            <div class="col-md-12">
               <ul class="nav nav-tabs">
                   <li class="active"><a href="#profile" data-toggle="tab">Profile</a> </li>
                   <li><a href="#upload" data-toggle="tab">Upload Slides & Assignments</a> </li>
                   <li><a href="#sub" data-toggle="tab">Submitted Assignments</a> </li>
                   <li><a href="#edit" data-toggle="tab">Edit Profile</a> </li>
               </ul>

                <div class="tab-content">
                    <div class="tab-pane active" id="profile"><!-- profile tab-->
                        <div class="col-md-8 col-md-offset-2"><br>
                            <div class="panel panel-info">
                                <div class="panel-heading">Lecturer Profile</div>
                                <div class="panel-body">
                                    <pre>
                                        <p>Name:</p>
                                        <P>Email:</P>
                                    </pre>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id="upload">    <!--upload tab-->
                        <div class="col-md-6 col-md-offset-3"><br>
                            <div class="panel panel-info">
                                <div class="panel-heading">Enter Following Details</div>
                                <div class="panel-body">
                                    <form class="form-horizontal" method="post" role="form" action="">
                                        <label>lecturer Name:</label>
                                        <input type="text" class="form-control" name="name">

                                        <label>Year:</label>
                                        <select class="form-control" name="year">
                                            <option disabled selected>Please select Year</option>
                                            <option value>Year 1</option>
                                            <option value>Year 2</option>
                                            <option value>Year 3</option>
                                        </select>

                                        <label>Select Faculty:</label>
                                        <select class="form-control" name="faculty">
                                            <option disabled selected>Please Select Faculty</option>
                                            <option value>Faculty of Computing</option>
                                            <option value>Faculty of Management</option>
                                            <option value>Faculty of Engineering</option>
                                        </select>

                                        <label>Course:</label>
                                        <select class="form-control" required name="course">
                                            <option disabled selected>Choose your Course</option>
                                            <option>BSc in Business Management(Human Resource Management)(Special)</option>
                                            <option>BSc in Business Management(Logistics Management)(Special)</option>
                                            <option>BSc in Business Management(Project Management)(Special)</option>
                                            <option>BSc in Business Management(Industrial Management)(Special)</option>
                                            <option>BSc(Hons) Marketing Management</option>
                                            <option>BSc Management Information Systems</option>
                                            <option>BSc(Hons)Computing</option>
                                            <option>BSc(Hons)Software Engineering</option>
                                            <option>BSc(Hons)Computer Networks</option>
                                            <option>BSc(Hons)Computer Security</option>
                                        </select>

                                        <label>Select File:</label>
                                        <input type="file" class="form-control" name="file"><br>
                                        <button type="submit" class="btn btn-default">Submit</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id="sub"><!-- submitted assignment-->
                        <div class="col-md-10 col-md-offset-1"><br>
                            <table class="table table-bordered table-bordered">
                                <thead>
                                <tr>
                                    <th>Student Id</th>
                                    <th>Student Name</th>
                                    <th>Date</th>
                                    <th>Assignment</th>
                                </tr>
                                </thead>

                                <tbody>
                                <tr>
                                    <td></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="tab-pane" id="edit"><!-- Edit profile-->
                        <div class="row"><br>
                            <div class="col-md-4 col-md-offset-2">
                                <div class="panel panel-info">
                                    <div class="panel-heading">Edit Information</div>
                                    <div class="panel-body">
                                        <form class="form form-horizontal" method="post" role="form" action="">
                                            <label>First Name:</label>
                                            <input type="text" class="form-control" name="fname">

                                            <label>Last Name:</label>
                                            <input type="text" class="form-control" name="lname">

                                            <label>Email:</label>
                                            <input type="email" class="form-control" name="email">
                                            <br>
                                            <button type="submit" class="btn btn-default">Submit</button>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="panel panel-info">
                                    <div class="panel-heading">Edit Password</div>
                                    <div class="panel-body">
                                        <form class="form-horizontal" method="post" role="form" action="">
                                            <label>Current Password:</label>
                                            <input type="password" class="form-control" name="cpass">

                                            <label>New Password:</label>
                                            <input type="password" class="form-control" name="npass">

                                            <label>Confirm New Password:</label>
                                            <input type="password" class="form-control" name="cnpass">
                                            <br>

                                            <button type="submit" class="btn btn-default">Submit</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>







    <?php include 'includes/footer.php';?>

    <script src="js/jquery-1.11.3.min.js"></script><!--Javascript file-->
    <script src="js/bootstrap.min.js"></script>
</body>
</html>