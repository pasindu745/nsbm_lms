<?php
error_reporting(E_ALL ^ E_NOTICE);

include 'functions/connection.php';
require 'functions/functions.php'
?>

<html>
<head>
    <title>Lecturer Signup</title>
    <?php include 'includes/head.php'; ?><!--css files-->
</head>
<body>

    <?php include 'includes/navbar_lec.php'; ?><!-- nav bar for lecturers login and sign up pages-->
    <br><br><br><br>

    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-info">
                    <div class="panel-heading"><strong>Please Enter Lecturer Details for Sign up</strong></div>
                    <div class="panel-body">
                        <?php
                        $submit=$_POST['submit'];
                        $fname=$_POST['fname'];
                        $lname=$_POST['lname'];
                        $email=$_POST['email'];
                        $remail=$_POST['remail'];
                        $pass=$_POST['pass'];
                        $repass=$_POST['repass'];
                        $date=date("y-m-d");

                        if($submit)
                        {
                            $mail_check=mysql_query("SELECT email FROM lec_signup WHERE email='$email'");
                            $count=mysql_num_rows($mail_check);

                            if($fname && $lname && $email && $remail && $pass && $repass)
                            {
                                if(strlen($fname)>20)
                                {
                                    echo "max value for first name is 20";
                                }
                                else
                                {
                                    if(strlen($lname)>30)
                                    {
                                        echo "max value for last name is 30";
                                    }
                                    else
                                    {
                                        if(strlen($email)>50)
                                        {
                                            echo "max value for email is 50";
                                        }
                                        else
                                        {
                                            if($email != $remail)
                                            {
                                                echo "Email does not match";
                                            }
                                            else
                                            {
                                                if(strlen($pass)>20 || strlen($pass)<6)
                                                {
                                                    echo "Password must between 6 to 20 characters";
                                                }
                                                else
                                                {
                                                    if($pass != $repass)
                                                    {
                                                        echo "Passwords do not match";
                                                    }
                                                    else
                                                    {
                                                        if($count != 0)
                                                        {
                                                            echo "Email has already taken.please try another one";
                                                        }
                                                        else
                                                        {
                                                            $pass=md5($pass);
                                                            $insert_query=mysql_query("INSERT INTO lec_signup VALUES ('' , '$fname' , '$lname' , '$email' , '$pass' , '$date') ");
                                                            echo "Success";
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                echo "Please fill all fields";
                            }

                        }

                        ?>


                        <form class="form-horizontal" method="post" role="form" action="lec_signup.php">
                            <label>First Name:</label>
                            <input type="text" class="form-control" name="fname">

                            <label>Last Name:</label>
                            <input type="text" class="form-control" name="lname">

                            <label>Email:</label>
                            <input type="email" class="form-control" name="email">

                            <label>Re enter Email:</label>
                            <input type="email" class="form-control" name="remail">

                            <label>Password:</label>
                            <input type="password" class="form-control" name="pass">

                            <label>Re enter Password:</label>
                            <input type="password" class="form-control" name="repass">

                            <br>
                            <input type="submit" name="submit" value="Sign Up" class="btn btn-info">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <?php include 'includes/footer.php';?>



<script src="js/jquery-1.11.3.min.js"></script><!--Javascript file-->
<script src="js/bootstrap.min.js"></script>
</body>
</html>