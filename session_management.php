<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Session management</title>
    <?php include 'includes/head.php'; ?><!--css files-->
</head>

<body>
    <?php include 'includes/navbar.php';?><!--navbarphp-->
    <br><br><br><br>

    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <?php include 'includes/panel.php';?><!-- pills-->
            </div>

            <div class="col-md-9">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#sesions" data-toggle="tab">Sessions</a> </li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane active" id="sessions"><br>
                        <table class="table table-bordered table-responsive">
                            <thead>
                            <tr>
                                <th>Session Id</th>
                                <th>User</th>
                                <th>Date</th>
                                <th>Login Time</th>
                                <th>Logout time</th>
                                <th>Date</th>
                                <th>IP address</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>





    <script src="js/jquery-1.11.3.min.js"></script><!--Javascript file-->
    <script src="js/bootstrap.min.js"></script>
</body>
</html>